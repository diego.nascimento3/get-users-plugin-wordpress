# get-users-plugin-wordpress

Este é um plugin simples para realizar uma solicitação get no WordPress, que expõe um endpoint de API para solicitar dados de usuários.

## Instalação

Baixe o arquivo do plugin e coloque-os no diretório wp-content/plugins/plugin-users ou baixe como zip e adicione dentro do wordpress na aba de plugins.

Ative o plugin por meio da interface de administração do WordPress.


## Detalhes do Endpoint

Método: GET
Permissão: Necessário ter o permissão de Administrador

## Formato da Resposta

O endpoint retorna um array de objetos de usuário com as seguintes propriedades:

- `id`: ID do usuário
- `username`: Nome de usuário
- `email`: Endereço de e-mail
- `registered`: Data de registro
- `display_name`: Nome de exibição
- `first_name`: Primeiro nome
- `last_name`: Sobrenome
- `user_url`: URL do usuário
- `description`: Descrição do usuário
- `roles`: Funções do usuário


## Exemplo de uso

```python
import requests
import base64

credentials = "user:passwordapi"
token = base64.b64encode(credentials.encode()).decode('utf-8')
header = {'Authorization': f'Basic {token}'}

url = "http://exemple.com/wp-json/plugin-users/v1/users"

response = requests.get(url, headers=header)
```
