<?php
/*
Plugin Name: User Data Plugin
Description: Plugin to return user data
Version: 1.0
Author: Diego Nascimento
*/

add_action('rest_api_init', function () {
  register_rest_route('plugin-users/v1', '/users/', array(
    'methods' => 'GET',
    'callback' => 'get_user_data',
    'permission_callback' => function () {
      return current_user_can('administrator');
    }
  ));
});

function get_user_data() {
  $users = get_users();
  $user_data = array();

  foreach ($users as $user) {
    $user_data[] = array(
      'id' => $user->ID,
      'username' => $user->user_login,
      'email' => $user->user_email,
      'registered' => $user->user_registered,
      'display_name' => $user->display_name,
      'first_name' => $user->first_name,
      'last_name' => $user->last_name,
      'user_url' => $user->user_url,
      'description' => $user->description,
      'roles' => $user->roles
    );
  }

  return $user_data;
}
?>